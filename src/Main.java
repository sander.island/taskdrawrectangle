import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        int width = 0;
        int height = 0;
        String str;
        Scanner scanner = new Scanner(System.in);

        // Enter the width
        System.out.println("Enter width...");
        String widthInput = scanner.nextLine();  // Read user input
        do {
            try
            {
                width = Integer.parseInt(widthInput);
                if(width <= 0)
                {
                    System.out.println("Width has to be a positive integer bigger than 0. Try again...");
                    widthInput = scanner.nextLine();
                }
            }
            catch (NumberFormatException e)
            {
                System.out.println("\"" + widthInput + "\"" + " is not a number!");
                widthInput = scanner.nextLine();
            }
        } while (width <= 0);

        // Enter the height
        System.out.println("Enter height...");
        String heightInput = scanner.nextLine();  // Read user input
        do {
            try
            {
                height = Integer.parseInt(heightInput);
                if(height <= 0)
                {
                    System.out.println("Height has to be a positive integer bigger than 0. Try again...");
                    heightInput = scanner.nextLine();
                }
            }
            catch (NumberFormatException e)
            {
                System.out.println("\"" + heightInput + "\"" + " is not a number!");
                heightInput = scanner.nextLine();
            }
        } while (height <= 0);

        // Enter the character
        System.out.println("Enter which character you want to use...");
        String strInput = scanner.nextLine();

        do {
            str = strInput;
            if (str.length() != 1)
            {
                System.out.println("You have to put in only one single character. Try again...");
                strInput = scanner.nextLine();
            }
        } while (str.length() != 1);

        // DRAW RECTANGLE
        System.out.println("Size " + width + " x " + height + ", using " + str);
        for (int i = 1; i <= height; i++)
        {
            for (int j = 1; j <= width; j++)
            {
                if (i == 1 || i == height || j == 1 || j == width)
                {
                    System.out.print(str);
                }
                else {
                    System.out.print(" ");
                }
            }
            System.out.println();
        }
    }
}